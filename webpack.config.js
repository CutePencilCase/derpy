const path = require('path')
const { CheckerPlugin, TsConfigPathsPlugin } = require('awesome-typescript-loader')
const nodeExternals = require('webpack-node-externals')

module.exports = {
    mode: 'development',
    target: 'node',
    entry: './src/index.ts',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'derpy.js'
    },
    resolve: {
        extensions: ['.ts', '.js', '.json'],
        plugins: [
            new TsConfigPathsPlugin()
        ],
        modules: ['mode_modules']
    },
    devtool: 'source-map',
    module: {
        rules: [
            { test: /\.ts$/, loader: 'awesome-typescript-loader' }
        ]
    },
    plugins: [new CheckerPlugin()],
    externals: [nodeExternals()]
}
