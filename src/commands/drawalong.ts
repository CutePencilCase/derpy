import { Message, TextChannel, User } from 'discord.js'
import { SubCommand, Callback } from 'derpy/modules/dispatcher'
import Drawalong from 'derpy/modules/drawalong'

// TODO: Generate topics on the fly (use the prompt module, maybe?)
const DEFAULT_TOPIC = 'Ponies!'

export const DrawalongCommand = new class extends SubCommand {
    description = 'Runs drawalongs'
    current?: Drawalong
}

DrawalongCommand.on('new', (args, message) => {
    if (!(message.channel instanceof TextChannel)) {
        message.channel.send("You can't run a drawalong here.")
        return
    }

    if (DrawalongCommand.current) {
        message.channel.send('A drawalong is already running!')
        return
    }

    let topic
    if (args.length > 0) {
        topic = args.join(' ')
    } else {
        topic = DEFAULT_TOPIC
    }

    DrawalongCommand.current = new Drawalong(message.author, message.channel, topic, () => {
        DrawalongCommand.current = undefined
    })
})

const withCurrent: (f: (drawalong: Drawalong, args: string[], user: User) => void) => Callback = f => {
    return (args, message) => {
        if (DrawalongCommand.current) {
            f(DrawalongCommand.current, args, message.author)
        } else {
            message.channel.send('There is no drawalong currently running!')
        }
    }
}

DrawalongCommand.with(withCurrent, on => {
    on('clear', current => {
        current.clear()
    })

    on('join', (current, _args, user) => {
        current.addUser(user)
    })

    on('leave', (current, _args, user) => {
        current.removeUser(user)
    })

    on('quit', (current, _args, user) => {
        current.removeUser(user)
    })

    on('topic', (current, args) => {
        if (args.length > 0) {
            current.topic = args.join(' ')
            current.channel.send(`Got it! The topic is now “${current.topic}”.`)
        } else {
            current.channel.send(`Current topic is “${current.topic}”.`)
        }
    })

    on('start', current => {
        current.start()
    })

    on('notify', (current, _args, user) => {
        current.notify(user)
    })

    on('boop', (current, _args, user) => {
        current.boop(user)
    })
})

DrawalongCommand.addHelp('new', 'Starts a new drawalong. You can optionally provide a topic.')
DrawalongCommand.addHelp('join', 'Join the drawalong')
DrawalongCommand.addHelp('leave', 'Quit the drawalong (alias: `quit`)')
DrawalongCommand.addHelp('topic', 'Show or changes topic')
DrawalongCommand.addHelp('start', 'Start the timer for a drawalong')
DrawalongCommand.addHelp('notify', 'Inform users that you are about to run a drawalong')
DrawalongCommand.addHelp('boop', 'Inform users that you opened a drawalong')

DrawalongCommand.helpMessage =
    "A drawalong is an activity where multiple people on the channel draw together, each on their own drawing. " +
    "It runs for 30 minutes where you can draw, and then everyone can share their drawing and can request for critics. " +
    "No need to rush: you don't have to have a finished product at the end!\n\n" +
    "Interested? Use `%da join` to join a running drawalong or `%da new` to start a new one."
DrawalongCommand.registerHelp()

export const BoopCommand = new class extends SubCommand { }

BoopCommand.with(withCurrent).on('drawalong', (current, _args, user) => {
    current.boop(user)
})
