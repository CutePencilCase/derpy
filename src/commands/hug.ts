import { Message, MessageMentions } from 'discord.js'
import { sample } from 'derpy/utils'
import { HUGS } from 'derpy/static'
import Command from 'modules/command';

const EVERYONE_MATCHES = ['everyone', 'everypony', 'everyponi', 'everypone', 'all', "y'all", 'channel']

export default class HugCommand implements Command {
    description: string = 'Initialize SnugglePone® 3000 protocols'

    handleMessage(args: string[], message: Message) {
        const users = [...new Set(args.filter(arg => arg.match(MessageMentions.USERS_PATTERN)))]
        const reply = (msg: string) => message.channel.send(msg)

        if (EVERYONE_MATCHES.find(match => args.includes(match))) {
            return reply(this.makeChannelHug(message))
        }

        if (users.find(user => user === message.client.user.toString())) {
            return reply("I can't hug myself, silly!")
        }

        // TODO: Currently, hugs are instant string responses. We could imagine having them do more
        // than just that by using little bit bore complex HugMakers. Delayed responses, more
        // interactivity, etc.
        if (users.length > 1) {
            reply(this.makeGroupHug(users))
        } else if (users.length > 0) {
            reply(this.makeHug(users[0]))
        } else {
            reply(this.makeHug(message.author.toString()))
        }
    }

    private makeGroupHug(users: string[]): string {
        const lastUser = users.pop()
        const names = `${users.join(', ')} and ${lastUser}`
        return sample(HUGS.BASIC)(names)
    }

    private makeHug(user: string): string {
        return sample(HUGS.BASIC)(user)
    }

    private makeChannelHug(message: Message): string {
        return sample(HUGS.CHANNEL)
    }
}
