import { Message } from 'discord.js'
import { sample, chance } from 'derpy/utils'
import { PONIES, LOCATIONS, SITUATIONS, SPECIES, COLORS } from 'derpy/static'
import { SubCommand } from 'derpy/modules/dispatcher'
import axios from 'axios'

const PromptCommand = new class extends SubCommand {
    description = 'Generates a drawing prompt to give you ideas'
}

PromptCommand.on(SubCommand.Empty, (_args, message) => {
    const ponies = sample(PONIES, chance(3) ? 2 : 1).join(' and ')
    const location = !chance(4) && sample(LOCATIONS)
    const situation = !chance(10) && sample(SITUATIONS)

    const prompt = [ponies, location, situation].filter(Boolean).join(', ')
    message.channel.send(`You should draw… ${prompt}!`)
})

const getOneOf = (list: string[]) => (_args: string[], message: Message) => {
    message.channel.send(`I suggest “${sample(list)}”.`)
}

PromptCommand.on('pony', getOneOf(PONIES))
PromptCommand.on('location', getOneOf(LOCATIONS))
PromptCommand.on('situation', getOneOf(SITUATIONS))

const DERPI_RANDOM_URL = 'https://derpibooru.org/search.json?q=safe,score.gte:500,!animated&random_image=y'
const DERPI_IMAGE_URL = (id: string) => `https://derpibooru.org/${id}.json`

PromptCommand.on('derpi', async (_args, message) => {
    const random = await axios.get(DERPI_RANDOM_URL)
    const image = await axios.get(DERPI_IMAGE_URL(random.data.id))

    const artists = image.data.tags
        .split(/,\s*/)
        .filter((t: string) => t.startsWith('artist:'))
        .map((t: string) => t.slice(7))
        .join(', ')

    message.channel.send(
        `Image: <https://derpibooru.org/${image.data.id}>\n` +
        `Artists: ${artists}\n\n` +
        `https:${image.data.representations.small}`
    )
})

PromptCommand.on('oc', (_args, message) => {
    const species = sample(SPECIES)
    const colors = sample(COLORS, chance(2) ? 2 : 1).join(' and ')

    message.channel.send(`I'm thinking of a ${colors} ${species} OC.`)
})

export default PromptCommand
