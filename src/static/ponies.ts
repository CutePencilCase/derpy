export default [
    'Twilight Sparkle', 'Rainbow Dash', 'Fluttershy', 'Pinkie Pie', 'Rarity', 'Applejack',
    // Royalty
    'Princess Celestia', 'Princess Luna', 'Princess Cadance',
    // Background
    'Derpy', 'Dinky', 'Lyra', 'Bon Bon', 'Big Macintosh',
    // Wonderbolts
    "Soarin'", 'Spitfire',
    // CMCs
    'Scootaloo', 'Sweetie Belle', 'Applebloom',
    // Not ponies
    'Spike', 'Gilda', 'Discord', 'Chrysalis', 'Thorax',
    // Non canon
    'Your pony/fursona', "Another participant's pony/fursona"
]
