type HugMaker = (names: string) => string

export default {
    BASIC: [
        names => `_is so happy to be able to hug ${names}!_`,
        names => `_extends her wings around ${names}_`,
        names => `_takes ${names} on an hug adventure_`,
        names => `_hugs warmly_\nI just have one question though, why dont't I hug ${names} more often? It's so great!`,
        names => `_hugs and nuzzles ${names}_`,
        names => `_moves ${names} on a little cloud for a hug in the sky_`,
        names => `ERROR: Target “${names}” is way too cute`,
        names => `_pounces on ${names}_`,
        names => `Just what I needed: A hug with ${names}.`,
        names => `_hugs ${names} around a big bowl of muffins_`,
        names => `Next time I'll feel sad, I'll remember hugging ${names} like this, and all my troubles will go away. :heart:`,
        names => `Ooooooh, I think I'll dream about hugging ${names} again tonight!`,
        names => `_recharges her batteries hugging ${names}_`,
        names => `Hugston, prepare final hug approach to ${names}, over.`,
        names => `Sweet hugs are made of this :notes:\nWho am I to disagree? :notes:\nI hugged ${names} on the seven seas :notes:\nEverypony's looking for such a thing :notes:`,
        names => `${names}: STOP! Hug time.`,
        names => `I'll make ${names} an offer they can't refuse: a hug with me.`,
        names => `:rotating_light: HUG EMERGENCY :rotating_light:\nI need 1 unit of hug for ${names}!`,
        names => `:ambulance: This is the hugbulance, coming through for ${names}!`,
        names => `_shares muffins with ${names}_`,
        names => `_has a surprise for ${names}: it's a hug!_`,
        names => `_hugs ${names} warmly_`,
        names => `_hugs ${names} forever_`,
        names => `_captures ${names} and gets them into her hug dungeon_`,
        names => `_hugs ${names} and gives them an A+ for these hug skills_`,
        names => `_is happy to be with the so-huggable ${names}_`,
        names => `_has missed hugging ${names} so much_`,
        names => `_jumps into ${names}'s arms_`,
        names => `_initializes the Hug-o-tron® 9000 and hits ${names}_`,
        names => `_gives ${names} a hug and a muffin_`,
        names => `_hugs ${names} and gets envious at these hugging skills_`,
        names => `_has waited so long to be able to finally hug ${names}_`,
        names => `_has dreamt of this for ages: a hug with ${names}_`,
        names => `_is honored of getting a hug from ${names}_`,
        names => `_would have to write a friendship letter after ${names} hugs her_`,
        names => `_hugs ${names} and faints from such an incredible experience_`,
        names => `_lifts ${names} into her arms for the hug of their life_`,
        names => `_had no idea hugging ${names} could be so great_`,
        names => `_falls from the sky right into ${names}'s arms_`,
        names => `_needs no horn to give ${names} a magical hug_`,
        names => `_grabs ${names} for a hug in the sky_`,
        names => `_wonders what kind of hug ${names} would like and proceed to try them all_`,
        names => `_has a book filled with pictures of ${names} hugging her_`,
        names => `_curls her wings around ${names}_`,
        names => `_floofs and proceeds to hug ${names}_`,
        names => `_curl her arms around ${names} and proceeds to hug forever_`,
    ] as HugMaker[],
    CHANNEL: [
        "*doesn't even know with whom to start! :dizzy_face:*",
        "My wings don't reach this wide! :astonished:",
        'A hug this big can only result in the best things. *hugs everyone*',
        'All these people? F-For me? *blush*',
        '*M-M-M-MONSTER HUG*',
    ]
}
