export default [
    // Actions
    'drawing', 'sleeping', 'eating', 'playing video games', 'playing board games', 'playing card games',
    'playing', 'arguing', 'being themselves', 'being bored', 'doing groceries', 'cleaning up',
    // Disguises
    'disguised as a dragon', 'disguised as a monster', 'disguised as another pony', 'disguised as a royalty',
    // Complex actions
    'meeting a new friend', 'meeting an old friend', 'meeting a celebrity', 'meeting their evil double',
    'meeting their younger selves',
    // Times
    'for Nightmare Night', "for Hearth's Warming Eve", 'for the Summer Sun Celebration', 'for Winter Wrap Up',
    'for Hearts and Hooves Day'
]
