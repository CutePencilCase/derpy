export default [
    'red',
    'blue',
    'green',
    'yellow',
    'purple',
    'orange',
    'gray',
    'black',
    'white',
]
