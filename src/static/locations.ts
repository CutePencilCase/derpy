export default [
    // Cities
    'in Ponyville', 'in Canterlot', 'in Manehattan', 'in Las Pegasus', 'in Cloudsdale',
    'in the Crystal Empire',
    // Places
    "in Twilight's Castle", 'in the Royal Palace', 'in the Library', 'at Sugarcube corner',
    'at Rainbow falls', 'at the Rock Farm',
    // Other places
    'in the Changeling Kingdom', 'in the Dragon Kingdom',
    // Generic
    'under a tree', 'in a parc', 'by the pond', 'by the sea', 'in the sky', 'in a cavern',
]
