import wu from 'wu'

export function sample<T>(source: T[]): T;
export function sample<T>(source: T[], count: number): T[];

export function sample<T>(source: T[], maybeCount?: number): T | T[] {
    const count = maybeCount || 1
    if (count >= source.length) {
        return maybeCount ? source : source[0]
    }

    const selectedIndices = new Set()

    while (count > selectedIndices.size) {
        let index = Math.floor(Math.random() * (source.length - selectedIndices.size))
        while (selectedIndices.has(index)) {
            ++index
            if (index >= source.length) {
                index = 0
            }
        }

        selectedIndices.add(index)
    }

    const selected = [...wu(selectedIndices).map(index => source[index])]
    return maybeCount ? selected : selected[0]
}

export function roll(faces: number): number {
    return Math.floor(Math.random() * faces)
}

export function chance(max: number): boolean {
    return roll(max) === 0
}
