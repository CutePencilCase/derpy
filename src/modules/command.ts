import { Message } from 'discord.js'

export default interface Command {
    description?: string

    handleMessage(args: string[], message: Message): void
}
