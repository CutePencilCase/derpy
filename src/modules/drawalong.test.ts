import Drawalong from './drawalong'

jest.useFakeTimers()

function makeUser(id: number, username: string): any {
    return { id, username, toString() { return `<@#${id}>` } }
}

describe(Drawalong, () => {
    const user: any = makeUser(1, 'Fluttershy')
    const channel: any = {
        send: jest.fn()
    }
    const callback = jest.fn()
    let drawalong

    beforeEach(() => {
        jest.clearAllMocks()
        drawalong = new Drawalong(user, channel, 'Pony', callback)
    })

    it('initializes', () => {
        expect(drawalong.isRunning).toBe(false)
        expect(drawalong.users).toEqual([user])
        expect(channel.send).toHaveBeenCalled()
    })

    describe('addUser', () => {
        it('adds new users', () => {
            drawalong.addUser(makeUser(2, 'Rainbow Dash'))

            expect(drawalong.users.length).toEqual(2)
            expect(channel.send).toHaveBeenCalledWith("You're in, Rainbow Dash!")
        })

        it('rejects already in users', () => {
            drawalong.addUser(user)

            expect(drawalong.users.length).toEqual(1)
            expect(channel.send).toHaveBeenCalledWith("You're already in, Fluttershy.")
        })
    })

    describe('removeUser', () => {
        beforeEach(() => {
            drawalong.addUser(makeUser(2, 'Rainbow Dash'))
            channel.send.mockClear()
        })

        it('removes users', () => {
            drawalong.removeUser(user)

            expect(drawalong.users.length).toEqual(1)
            expect(channel.send).toHaveBeenCalledWith("You're out, Fluttershy.")
        })

        it("won't remove an unknown user", () => {
            drawalong.removeUser(makeUser(3, 'Twilight Sparkle'))

            expect(drawalong.users.length).toEqual(2)
            expect(channel.send).toHaveBeenCalledWith("You're already out, Twilight Sparkle.")
        })

        it('ends the drawalong on last user removed', () => {
            // Clone the array since we mutate it in removeUser
            Array.from(drawalong.users).forEach(user => drawalong.removeUser(user))

            expect(drawalong.users.length).toEqual(0)
            expect(channel.send).toHaveBeenCalledWith(
                "You're out, Rainbow Dash.\n" +
                "Since there are no users left, I clear the drawalong. See you next time!"
            )
        })
    })

    describe('start', () => {
        it('runs the drawalong', () => {
            drawalong.start()

            expect(drawalong.isRunning).toBe(true)
            expect(channel.send).toHaveBeenCalledWith(
                "<@#1>\n**Drawalong started!** Topic is “Pony”.\n\n" +
                "Drawalong ends at xx:30."
            )

            channel.send.mockClear()
            jest.runAllTimers()
            expect(channel.send).toHaveBeenCalledTimes(3)
            expect(channel.send).toHaveBeenCalledWith('Ten minutes remaining! End time is xx:30.')
            expect(channel.send).toHaveBeenCalledWith('**Five minutes remaining!** End time is xx:30.')
            expect(channel.send).toHaveBeenCalledWith(
                '[](/derpystop)\n<@#1>: Drawalong ended!\n\n' +
                'Share your art with others and ask for critique if you want it.\n' +
                'You can run a new round with `%da start` or clear the drawalong with `%da clear`.'
            )
            expect(drawalong.isRunning).toBe(false)
        })

        it("can't be started twice", () => {
            drawalong.start()
            channel.send.mockClear()

            drawalong.start()
            expect(channel.send).toHaveBeenCalledWith('The drawalong is already running.')
        })
    })

    describe('clear', () => {
        it('clears the drawalong', () => {
            drawalong.clear()
            expect(channel.send).toHaveBeenCalledWith("Cleared. Hope we run a new drawalong soon!")
            expect(callback).toHaveBeenCalledWith(channel)
        })

        it("can't clear a running drawalong", () => {
            drawalong.start()
            channel.send.mockClear()

            drawalong.clear()
            expect(channel.send).toHaveBeenCalledWith("You can't clear a running drawalong.")
        })
    })

    describe('boop', () => {
        it('boops', () => {
            drawalong.boop(user)
            expect(channel.send).toHaveBeenCalledWith(
                '**Fluttershy wants to do a drawalong!** Topic is “Pony”.\n@here: Use `%da join` if you wish to participate!'
            )
        })

        it("won't work during the drawalong", () => {
            drawalong.start()
            channel.send.mockClear()

            drawalong.boop(user)
            expect(channel.send).toHaveBeenCalledWith("Sorry, you can't do this while the drawalong is not running.")
        })

        it("won't work for an external user", () => {
            drawalong.boop(makeUser(3, 'Twilight Sparkle'))
            expect(channel.send).toHaveBeenCalledWith("Sorry, you can do this only if you participate in the drawalong.")
        })
    })

    describe('notify', () => {
        it('notifies', () => {
            drawalong.notify(user)
            expect(channel.send).toHaveBeenCalledWith(
                '**The drawalong is about to start!** Topic is “Pony”.\n<@#1>: Are you ready?'
            )
        })
    })
})
