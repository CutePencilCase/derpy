import { TextChannel, User } from 'discord.js'
import { DateTime, DurationObject } from 'luxon'

type ClearCallback = (_: TextChannel) => void

export default class Drawalong {
    static readonly DURATION = { minutes: 30 }
    static readonly WARNINGS = [
        [{ minutes: 10 }, 'Ten minutes remaining!'],
        [{ minutes: 5 }, '**Five minutes remaining!**'],
    ]

    public readonly channel: TextChannel
    public readonly users: User[]
    public topic: string
    readonly callback?: ClearCallback
    endTime?: DateTime
    readonly timers: NodeJS.Timer[]

    constructor(author: User, channel: TextChannel, topic: string, callback?: ClearCallback) {
        this.channel = channel
        this.users = [author]
        this.topic = topic
        this.callback = callback
        this.timers = []

        this.channel.send(`**New drawalong started!** Topic is “${topic}”.`)
    }

    public get isRunning(): boolean {
        return Boolean(this.endTime)
    }

    public boop(user: User) {
        this._runProtected(user, () => {
            this.channel.send(
                `**${user.username} wants to do a drawalong!** Topic is “${this.topic}”.\n` +
                '@here: Use `%da join` if you wish to participate!'
            )
        })
    }

    public addUser(user: User) {
        if (this.users.find(item => item.id === user.id)) {
            this.channel.send(`You're already in, ${user.username}.`)
        } else {
            this.users.push(user)
            this.channel.send(`You're in, ${user.username}!`)
        }
    }

    public removeUser(user: User) {
        const index = this.users.findIndex(item => item.id === user.id)

        if (index >= 0) {
            this.users.splice(index, 1)

            if (this.users.length === 0) {
                this._clear()
                this.channel.send(
                    `You're out, ${user.username}.\n` +
                    "Since there are no users left, I clear the drawalong. See you next time!"
                )
            } else {
                this.channel.send(`You're out, ${user.username}.`)
            }
        } else {
            this.channel.send(`You're already out, ${user.username}.`)
        }
    }

    public notify(user: User) {
        this._runProtected(user, () => {
            this.channel.send(`**The drawalong is about to start!** Topic is “${this.topic}”.\n${this._mentions()}: Are you ready?`)
        })
    }

    public start() {
        if (this.endTime) {
            this.channel.send('The drawalong is already running.')
            return
        }

        this.endTime = DateTime.utc().plus(Drawalong.DURATION)
        const endTimeString = this.endTime.minute.toString().padStart(2, '0')

        Drawalong.WARNINGS.forEach(([time, text]) => {
            const timer = this.endTime!.minus(time as DurationObject).diffNow().as('milliseconds')
            this._makeTimer(timer, () => {
                this.channel.send(text + ` End time is xx:${endTimeString}.`)
            })
        })

        this._makeTimer(this.endTime.diffNow().as('milliseconds'), () => {
            this.endTime = undefined
            this.channel.send(
                `[](/derpystop)\n${this._mentions()}: Drawalong ended!\n\n` +
                'Share your art with others and ask for critique if you want it.\n' +
                'You can run a new round with `%da start` or clear the drawalong with `%da clear`.'
            )
        })

        this.channel.send(
            `${this._mentions()}\n` +
            `**Drawalong started!** Topic is “${this.topic}”.\n\n` +
            `Drawalong ends at xx:${endTimeString}.`
        )
    }

    public clear() {
        if (this.isRunning) {
            this.channel.send("You can't clear a running drawalong.")
            return
        }

        this._clear()
        this.channel.send("Cleared. Hope we run a new drawalong soon!")
    }

    private _runProtected(user: User, callback: () => void) {
        if (!this.users.find(item => item.id === user.id)) {
            this.channel.send('Sorry, you can do this only if you participate in the drawalong.')
            return
        }

        if (this.isRunning) {
            this.channel.send("Sorry, you can't do this while the drawalong is not running.")
            return
        }

        callback()
    }

    private _clear() {
        this.timers.forEach(timer => clearTimeout(timer))
        this.timers.splice(0)

        if (this.callback) {
            this.callback(this.channel)
        }
    }

    private _makeTimer(duration: number, callback: () => void) {
        const timer = setTimeout(() => {
            const index = this.timers.indexOf(timer)
            this.timers.splice(index, 1)

            callback()
        }, duration)

        this.timers.push(timer)
    }

    private _mentions(): string {
        return this.users.map(user => user.toString()).join(', ')
    }
}
