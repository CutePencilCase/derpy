import { Dispatcher } from './dispatcher'

describe(Dispatcher, () => {
    beforeEach(() => {
        jest.resetAllMocks()
    })

    it('dispatches commands', () => {
        const dispatcher = new Dispatcher
        const callback = { handleMessage: jest.fn() }

        dispatcher.on('hello', callback)

        const message: any = { content: 'hello world' }
        dispatcher.handle(message)

        expect(callback.handleMessage).toHaveBeenCalledWith(['world'], message)
    })

    describe('with prefix', () => {
        const dispatcher = new Dispatcher
        const callback = { handleMessage: jest.fn() }

        dispatcher.prefix = '^'
        dispatcher.on('hello', callback)

        it('dispatches command with a prefix', () => {
            const message: any = { content: '^hello world' }
            dispatcher.handle(message)

            expect(callback.handleMessage).toHaveBeenCalledWith(['world'], message)
        })

        it('ignores messages without prefixes', () => {
            const message: any = { content: 'hello' }
            dispatcher.handle(message)

            expect(callback.handleMessage).not.toHaveBeenCalled()
        })
    })
})
