import { Message, Client, TextChannel, GroupDMChannel, DMChannel } from 'discord.js'
import Command from 'derpy/modules/command'
import wu from 'wu'

export type Callback = (args: string[], message: Message) => void

interface IDispatcher<T> {
    on(method: string, callback: T): void
}

export class Dispatcher implements IDispatcher<Callback> {
    static readonly Default = Symbol('default')
    static readonly Empty = Symbol('empty')

    methods: Map<string | Symbol, Callback | Command> = new Map()
    prefix?: string
    helpMessage?: string

    constructor(client?: Client) {
        if (client) {
            client.on('message', message => this.handle(message))
        }
    }

    public on(method: string | Symbol, callback: Callback | Command) {
        this.methods.set(method, callback)
    }

    public addHelp(method: string, message: string) {
        const callback: any = this.methods.get(method)!
        callback.description = message
    }

    public handle(message: Message) {
        if (!this.prefix || message.content.startsWith(this.prefix)) {
            const args = (this.prefix ? message.content.slice(this.prefix.length) : message.content).split(/\s+/)
            if (args.length < 1) {
                return
            }
            this._handle(args, message)
        }
    }

    public with<U>(transformer: (u: U) => Callback, callback?: (on: (method: string, u: U) => void) => void): IDispatcher<U> {
        const on = (method: string, callback: U) => {
            this.on(method, transformer(callback))
        }

        if (callback) {
            callback(on)
        }

        return { on }
    }

    public help(channel: TextChannel | DMChannel | GroupDMChannel) {
        let message = this.helpMessage ? this.helpMessage + '\n\n' : ''

        message += '**Available commands**:'
        message = wu(this.methods).reduce((acc, [name, callback]): string => {
            if ('description' in callback && callback.description) {
                return acc + `\n\`${name}\`: ${callback.description}`
            }

            return acc
        }, message)

        channel.send(message)
    }

    public registerHelp() {
        this.on('help', (_args, message) => {
            this.help(message.channel)
        })
    }

    protected _handle(args: string[], message: Message) {
        const command = args.shift() || Dispatcher.Empty

        const callback = this.methods.get(command) || this.methods.get(Dispatcher.Default)
        if (callback) {
            if (callback instanceof Function) {
                callback(args, message)
            } else {
                callback.handleMessage(args, message)
            }
        }
    }
}

export class SubCommand extends Dispatcher implements Command {
    handleMessage(args: string[], message: Message) {
        this._handle(args, message)
    }
}
