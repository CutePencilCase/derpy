import { env, exit } from 'process'
import { Client } from 'discord.js'
import winston from 'winston'
import { Dispatcher } from 'derpy/modules/dispatcher'
import HugCommand from 'derpy/commands/hug'
import PromptCommand from 'derpy/commands/prompt'
import { DrawalongCommand, BoopCommand } from 'derpy/commands/drawalong'

const logger = winston.createLogger({
    level: 'debug',
    transports: [
        new winston.transports.Console({ format: winston.format.simple() })
    ]
})

if (!env.DISCORD_TOKEN) {
    logger.error('DISCORD_TOKEN not set.')
    exit(1)
}

const client = new Client()
const dispatcher = new Dispatcher(client)
dispatcher.prefix = '%'
dispatcher.helpMessage =
    "[](/derpyhasmuffin)\nHello! I'm **Derpy**, here to serve diverse functions and sometimes hug you warmly.\n" +
    "My care is taken of by *Adædra*. You can ask questions about me here or directly in private messages with him.\n" +
    "I'm open source! You can find my source code and contribute at <https://gitlab.com/adaedra/derpy>."
dispatcher.registerHelp()

dispatcher.on('hug', new HugCommand)
dispatcher.on('prompt', PromptCommand)
dispatcher.on('da', DrawalongCommand)
dispatcher.on('boop', BoopCommand)

client.on('ready', () => {
    logger.debug(`Logged in as ${client.user.tag}`)
})

client.login(env.DISCORD_TOKEN)
