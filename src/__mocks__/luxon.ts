import { DateTime as Upstream } from 'luxon'

export const DateTime = {
    utc() {
        return Upstream.fromISO('2010-10-10T10:00Z')
    }
}
