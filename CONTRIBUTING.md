# How to contribute

Hello! Thank you for considering contributing to Derpy. I consider her to be
MLPDS's bot and wish for everyone in the server to be able to give a helping
hoof.

Don't worry if you're not technical, you can still help, there is some for
everyone!

I have a development Discord for expanded discussion if you want:
[Join!](https://discord.gg/Y3DMDuK)

___

Here are the different ways you can contribute:

## Code

The most obvious is to participate to code. There are many ways Derpy can be
improved, either by fixing bugs or adding features. You can take a look at
the issues to see what there is to do.

To code on Derpy, use the fork button to make a copy of the repository on
GitLab you can work on, code your feature on a branch, then use GitLab to
do a pull request to the main repository. It will then be reviewed, and once
is good, will be integrated into Derpy.

If you work on an issue, assign yourself on it so other people know there is
already work on it. It doesn't mean you can't work on it: discuss with the
assignee and see if you can work together!

## Issues

It's not because you can't code that you can't contribute ideas or bug reports.
If you think of a way to improve Derpy, don't be afraid to open an issue!
Describe quickly what the enhancement is. It can then be discussed by other
interested people and eventually be taken by another developer.

## Static data

Derpy uses some static data (files in `src/static`) which are often just bits
of texts for other modules to use (e.g. ponies names, hug strings). The format
of the file should not be difficult to work out even without much of a
technical background. Don't hesitate to ask if you want to add something but
are not sure how to do it. Don't worry, there is no way you can break the
Derpy running on the real server, so you can experiment all you want.
