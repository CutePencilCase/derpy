# Derpy

Adorable Discord bot currently used in MLPDS Discord.

See also [Contribution guide](CONTRIBUTING.md).

## Development

You will need NodeJS and NPM installed on your computer. They often come
together, you have to see how your operating system handles it.

Install dependencies with `npm i`.

You can compile Derpy with `npm run build` which creates the `dist/derpy.js` file.

To run her, you will need to register a bot and get a bot token through
[Discord](https://discordapp.com/developers/applications/me). Set the
`DISCORD_TOKEN` environment variable and run Derpy with `npm start`. She
should connect and respond on any server she is on.

Run tests with `npm run test`.

During development, you can have compilation running in watching mode with
`npm run watch` and have tests run in watch mode with `npm run test:watch`.

[](/derpyhasmuffin)
